FROM java:8
ADD ./target/taskmanager-1.0.0.war .
EXPOSE 8080
ENTRYPOINT exec /usr/bin/java -jar ./taskmanager-1.0.0.war
