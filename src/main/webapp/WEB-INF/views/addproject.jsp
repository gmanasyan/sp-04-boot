<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Добавить проект</title>
    <link rel="stylesheet" href="assets/css/style.css">
<body>

<div class="header">
    <a href="projects">Projects</a>
    <a href="tasks">Tasks</a>
</div>

<div class="box">

<h2>Add Project</h2>

    <form method="post" action="add-project">
        <dl>
            <dt>Name of project:</dt>
            <dd><input type="text" value="" size=40 name="name" required></dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><input type="date" name="dateBegin" required></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><input type="date" name="dateEnd" required></dd>
        </dl>
        <button type="submit">Save</button>
        <button onclick="window.history.back()" type="button">Cancel</button>
    </form>

</div>
</body>
</html>