<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Main Page</title>
    <link rel="stylesheet" href="assets/css/style.css">
<body>

<div class="header">
    <a href="projects">Projects</a>
    <a href="tasks">Tasks</a>
</div>

<div class="box">

<h2>Tasks</h2>

<table border="1" cellpadding="10" cellspacing="0">

    <thead>
        <tr>
            <th>#</th>
            <th>ID</th>
            <th>Project</th>
            <th>Task Name</th>
            <th>Description</th>
            <th>View</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>

    <c:set var="count" value="0" scope="page" />

    <c:forEach items="${tasks}" var="task">
        <jsp:useBean id="task" type="ru.volnenko.se.entity.Task"/>
        <tr>
            <td>
                    <c:set var="count" value="${count + 1}" scope="page"/>
                    ${count}
            </td>
            <td>
                    ${task.id}
            </td>
            <td>
                    ${task.project.name}
            </td>
            <td>
                    ${task.name}
            </td>
            <td>
                    ${task.description}
            </td>
            <td>
                <a href="view-task?taskId=${task.id}">View</a>
            </td>
            <td>
                <a href="edit-task?taskId=${task.id}">Edit</a>
            </td>
            <td>
                <a href="remove-task?taskId=${task.id}">Remove</a>
            </td>
        </tr>
    </c:forEach>
</table>
<br/><br/>
<a class="button" href="add-task">Add Task</a>
</div>
</body>
</html>