<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Просмотр задачи</title>
    <link rel="stylesheet" href="assets/css/style.css">
<body>

<div class="header">
    <a href="projects">Projects</a>
    <a href="tasks">Tasks</a>
</div>

<div class="box">

<h2>View Task</h2>

    <form method="post" action="edit-project">
        <dl>
            <dt>Task ID:</dt>
            <dd>${task.id}</dd>
        </dl>
        <dl>
            <dt>Project Name:</dt>
            <dd>${task.project.name}</dd>
        </dl>
        <dl>
            <dt>Task Name:</dt>
            <dd>${task.name}</dd>
        </dl>
        <dl>
            <dt>Task Description:</dt>
            <dd>${task.description}</dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><fmt:formatDate value="${task.dateBegin}" pattern="yyyy-MM-dd"/></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><fmt:formatDate value="${task.dateEnd}" pattern="yyyy-MM-dd"/></dd>
        </dl>
    </form>
    <a class="button" onclick="window.history.back()">Back</a>
</div>
</body>
</html>