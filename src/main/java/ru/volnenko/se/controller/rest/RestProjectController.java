package ru.volnenko.se.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

import java.util.List;

@RestController
@RequestMapping(value = "/api/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/")
    public List<Project>  projects() {
        List<Project> projects = projectService.getListProject();
        return projects;
    }

    @GetMapping("/{id}")
    public Project viewProject(@PathVariable("id") String projectId ) {
        Project project = projectService.getProjectById(projectId);
        return project;
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addProject(@RequestBody Project project) {
        projectService.merge(project);
    }

    @PutMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editProject(@RequestBody Project project) {
        return projectService.existById(project.getId()) ?
                ResponseEntity.ok(projectService.merge(project)) : ResponseEntity.badRequest().build();
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProject(@PathVariable String id) {
        try {
            projectService.removeProjectById(id);
        } catch (Exception e) {
            System.out.println("Log: attempt to delete non existing project");
        }
    }

}
